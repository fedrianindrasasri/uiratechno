<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;

class DataMahasiswa extends Controller
{
    var $admin;
    var $mahasiswa;
    var $dosen;

    public function role()
    {
        $this->admin = Menu::get();
        $this->mahasiswa = Menu::where('role', 'mahasiswa')->get();
        $this->dosen = Menu::where('role', 'dosen')->get();
    }
    public function index()
    {
        $this->role();
        $admin = $this->admin;
        $mahasiswa = $this->mahasiswa;
        $dosen = $this->dosen;
        return view('dataMahasiswa', compact('admin', 'mahasiswa', 'dosen'));
    }
}

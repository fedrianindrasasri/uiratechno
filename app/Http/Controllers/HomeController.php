<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Menu;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    var $admin;
    var $mahasiswa;
    var $dosen;

    public function role()
    {
        $this->admin = Menu::get();
        $this->mahasiswa = Menu::where('role', 'mahasiswa')->get();
        $this->dosen = Menu::where('role', 'dosen')->get();
    }
    public function index()
    {
        $this->role();
        $admin = $this->admin;
        $mahasiswa = $this->mahasiswa;
        $dosen = $this->dosen;
        return view('home', compact('admin', 'mahasiswa', 'dosen'));
    }


    public function role2()
    {
       $admin = Menu::get();
       $mahasiswa = Menu::where('role', 'mahasiswa')->get();
       $dosen = Menu::where('role', 'dosen')->get();

    }
}

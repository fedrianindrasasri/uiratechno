<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/data-mahasiswa', [App\Http\Controllers\DataMahasiswa::class, 'index'])->name('datamahasiswa');
Route::get('/data-dosen', [App\Http\Controllers\DataDosen::class, 'index'])->name('datadosen');
Route::get('/data-matakuliah', [App\Http\Controllers\DataMataKuliah::class, 'index'])->name('datamatakuliah');

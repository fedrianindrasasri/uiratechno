<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Fedrian',
            'username'=> 'fedrian-mahasiswa',
            'email' => 'fedrian@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'mahasiswa'
        ]);

        User::create([
            'name' => 'Indra',
            'username' => 'fedrian-dosen',
            'email' => 'indra@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'dosen'
        ]);

        User::create([
            'name' => 'Sasri',
            'username' => 'fedrian-admin',
            'email' => 'sasri@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'admin'
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;


class MenuSeedd extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::create([
            'menu' => 'Data Mahasiswa',
            'route' => "{{ route('datamahasiswa') }}",
            'role' => 'dosen',
        ]);

        Menu::create([
            'menu' => 'Data Mata Kuliah',
            'route' => "{{ route('datamatakuliah') }}",
            'role' => 'mahasiswa',
        ]);

        Menu::create([
            'menu' => 'Data Dosen',
            'route' => "{{ route('datadosen') }}",
            'role' => 'admin',
        ]);

    }
}
